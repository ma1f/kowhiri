using Cassette;
using Cassette.Scripts;
using Cassette.Stylesheets;

namespace Kowhiri.Web
{
    /// <summary>
    /// Configures the Cassette asset bundles for the web application.
    /// </summary>
    public class CassetteBundleConfiguration : IConfiguration<BundleCollection>
    {
        public void Configure(BundleCollection bundles)
        {
            // TODO: Configure your bundles here...
            // Please read http://getcassette.net/documentation/configuration

            bundles.Add<ScriptBundle>("Scripts/modernizr-2.8.3.js", bundle => { bundle.PageLocation = "head"; });
            //bundles.Add<ScriptBundle>("Isotope", "Scripts/jquery.isotope.min.js");

            bundles.Add<StylesheetBundle>("Stylesheets/css/main.css");


            bundles.Add<ScriptBundle>("js");
            bundles.Add<ScriptBundle>("Scripts");
            bundles.Add<ScriptBundle>("validation",
                "~/Scripts/jquery.validate.js",
                "~/Scripts/jquery.validate.unobtrusive.js");

        }
    }
}