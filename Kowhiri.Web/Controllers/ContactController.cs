using System;
using System.Configuration;
using System.Net.Mail;
using System.Web.Mvc;
using Kowhiri.Web.Extensions;
using Kowhiri.Web.Helpers;
using Kowhiri.Web.Models;
using Umbraco.Web.Mvc;

namespace Kowhiri.Web.Controllers
{
    public class ContactController : SurfaceController
    {
        // GET: Contact
        public ActionResult GetForm()
        {
            return PartialView("Index", new ContactViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PostForm(ContactViewModel model)
        {
            var reCapture = ReCaptcha.Validate(model.RecaptchaResponse);

            if (!reCapture.Success)
            {
                TempData["validationMessage"] = "Recapture validation failed";
                return Json(new
                {
                    success = false,
                    html = this.RenderPartialViewToString("Index", model)
                });
            }

            //Check if the dat posted is valid (All required's & email set in email field)
            if (!ModelState.IsValid)
            {
                //Not valid - so lets return the user back to the view with the data they entered still prepopulated
                return Json(new
                {
                    success = false,
                    html = this.RenderPartialViewToString("Index", model)
                });
            }

            //Generate an email message object to send
            MailMessage email = new MailMessage(ConfigurationManager.AppSettings["Kowhiri.ContactFromEmail"], ConfigurationManager.AppSettings["Kowhiri.ContactToEmail"])
            {
                Subject = ConfigurationManager.AppSettings["Kowhiri.ContactSubject"],
                Body = $"Name : {model.FirstName} <br/> Phone : {model.PhoneNumber} <br/> Email : {model.Email} <br/> Message : {model.Enquiry}"
            };

            try
            {
                //Connect to SMTP credentials set in web.config
                SmtpClient smtp = new SmtpClient();
                //Try & send the email with the SMTP settings
                smtp.Send(email);
            }
            catch (Exception ex)
            {
                TempData["validationMessage"] = "Failed to send email";
                return Json(new
                {
                    success = false,
                    html = this.RenderPartialViewToString("Index", model),
                    message = ex.Message
                });

            }

            //Update success flag (in a TempData key)
            TempData["IsSuccessful"] = true;

            //All done - lets redirect to the current page & show our thanks/success message
            return Json(new
            {
                success = true,
                html = this.RenderPartialViewToString("Index", model)
            });
        }

    }
}