using System.Web.Mvc;
using DevTrends.MvcDonutCaching;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace Kowhiri.Web.Controllers
{
    public class ContentController : SurfaceController
    {
        // the cache is cleared upon change in the CMS
        [DonutOutputCache(CacheProfile = "OneHour")]
        public ActionResult Index(int id, object model = null, string viewName = "Index")
        {
            ViewBag.Content = Umbraco.TypedContent(id);

            if (ViewBag.Content == null)
            {
                Response.StatusCode = 404;
                return new HttpNotFoundResult();
            }

            if (viewName == null)
            {
                viewName = ViewBag.Content.ContentType.Alias;
            }

            return PartialView(viewName, model);
        }

      
    }
}