using System.Linq;
using DevTrends.MvcDonutCaching;
using Umbraco.Core;
using Umbraco.Core.Events;
using Umbraco.Core.Models;
using Umbraco.Core.Publishing;
using Umbraco.Core.Services;

namespace Kowhiri.Web.Events
{
    public class UmbracoEvents : ApplicationEventHandler
    {
        public UmbracoEvents()
        {
            ContentService.Published += PublishedHandler;
            ContentService.UnPublished += UnPublishedHandler;
            ContentService.Trashed += TrashedHandler;
        }

        private void PublishedHandler(IPublishingStrategy sender, PublishEventArgs<IContent> args)
        {
            umbraco.library.RefreshContent();
            new OutputCacheManager().RemoveItems();

            var item = args.PublishedEntities.FirstOrDefault(x => x.HasProperty("content"));

            if (item != null)
            {
                var emptyPtag = "<p> </p>";
                var content = item.GetValue<string>("content");
                var index = content.LastIndexOf(emptyPtag);

                if (index == content.Length - emptyPtag.Length)
                    item.SetValue("content", content.Substring(0, index));
            }
        }

        private void UnPublishedHandler(IPublishingStrategy sender, PublishEventArgs<IContent> args)
        {
            new OutputCacheManager().RemoveItems();
        }

        private void TrashedHandler(IContentService service, MoveEventArgs<IContent> args)
        {
            new OutputCacheManager().RemoveItems();
        }
    }
}