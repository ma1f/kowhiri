$(document).ready(function () {

	function showHideMobileMenu(e) {
	    e.preventDefault();
	    $("nav").slideToggle("slow", "swing");
	    $("a.lines-link").toggleClass("close");
	    $("a.login").removeClass("button");
	}

    // scroll to "pricing" section
	$(".btn-pricing").click(function () {
	    $("html, body").animate({ scrollTop: $(".pricing-outlines").offset().top }, 500);
	});

	// mobile "MENU" trigger
	$("#responsive-nav-toggle").on('click', showHideMobileMenu);

	$( ".accordion" ).accordion({
	    collapsible: true,
	    heightStyle: "content"
    });

    //Cross browser placeholder text
	if (!Modernizr.input.placeholder) {
	    $('[placeholder]').focus(function () {
	        var input = $(this);
	        if (input.val() == input.attr('placeholder')) {
	            input.val('');
	            input.removeClass('placeholder');
	        }
	    }).blur(function () {
	        var input = $(this);
	        if (input.val() == '' || input.val() == input.attr('placeholder')) {
	            input.addClass('placeholder');
	            input.val(input.attr('placeholder'));
	        }
	    }).blur();
	    $('[placeholder]').parents('form').submit(function () {
	        $(this).find('[placeholder]').each(function () {
	            var input = $(this);
	            if (input.val() == input.attr('placeholder')) {
	                input.val('');
	            }
	        })
	    });
	}

});