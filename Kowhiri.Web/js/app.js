
(function() {


    $('.contact-page').on('submit', 'form', handleContactSubmit);

    function handleContactSubmit(e) {
        e.preventDefault();

        var form = this;
        var success = false;

        if ($(form).valid()) {

            $.ajax({
                type: "POST",
                url: '/umbraco/surface/contact/PostForm',
                data: $(form).serialize(),
                success: function (data) {
                    success = true;
                    $('.ajax-replace').html(data.html);

                    if (!data.success) {
                        onloadCallback(); // re-start recapture
                    }
                }
            }).always(function() {
                if (!success) {
                }
            });
        }
    }

})();