using System.ComponentModel.DataAnnotations;

namespace Kowhiri.Web.Models
{
    public class ContactViewModel
    {
        [Display(Name = "Name")]
        [Required]
        public string FirstName { get; set; }


        [Display(Name = "Phone")]
        public string PhoneNumber { get; set; }

        [EmailAddress]
        [Required]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Message")]
        public string Enquiry { get; set; }

        [Required(ErrorMessage = "You must confirm you are not a robot")]
        public string RecaptchaResponse { get; set; }
    }
}