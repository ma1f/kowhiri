﻿using System.Collections.Generic;
using Kowhiri.Web.Utilities;
using Newtonsoft.Json;

namespace Kowhiri.Web.Helpers
{
    public class ReCaptcha
    {
        public static ReCaptcha Validate(string encodedResponse)
        {
            var client = new System.Net.WebClient();

            string secretKey = AppSettings.Get<string>("reCAPTCHA_Secret");

            var googleReply = client.DownloadString($"https://www.google.com/recaptcha/api/siteverify?secret={secretKey}&response={encodedResponse}");

            return JsonConvert.DeserializeObject<ReCaptcha>(googleReply);

        }

        [JsonProperty("success")]
        public bool Success { get; set; }


        [JsonProperty("error-codes")]
        public List<string> ErrorCodes { get; set; }
    }

}