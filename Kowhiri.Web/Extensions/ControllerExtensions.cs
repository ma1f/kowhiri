using System.IO;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace Kowhiri.Web.Extensions
{
    public static class ControllerExtensions
    {
        public static JsonNetResult JsonResult(this SurfaceController controller, object data)
        {
            return new JsonNetResult
            {
                Data = data
            };
        }

        public static string RenderPartialViewToString(this Controller controller)
        {
            return controller.RenderPartialViewToString(null, null);
        }

        public static string RenderPartialViewToString(this Controller controller, string viewName)
        {
            return controller.RenderPartialViewToString(viewName, null);
        }

        public static string RenderPartialViewToString(this Controller controller, object model)
        {
            return controller.RenderPartialViewToString(null, model);
        }

        public static string RenderPartialViewToString(this Controller controller, string viewName, object model, bool reuseContextData = true)
        {
            if (string.IsNullOrEmpty(viewName))
            {
                viewName = controller.ControllerContext.RouteData.GetRequiredString("action");
            }

            var tempData = reuseContextData ? controller.TempData : new TempDataDictionary(); 
            var viewData = controller.ViewData;
            
            if (reuseContextData)
            {
                viewData.Model = model;
            }
            else
            {
                viewData = new ViewDataDictionary { Model = model };
            }

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                var viewContext = new ViewContext(controller.ControllerContext, viewResult.View, viewData, tempData, sw);

                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }
    }
}
