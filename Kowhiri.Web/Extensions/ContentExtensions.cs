using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace Kowhiri.Web.Extensions
{
    public static class ContentExtensions
    {
        public static MvcHtmlString GetImage(this IPublishedContent content, string fieldName)
        {
            if (content.HasValue(fieldName))
            {
                var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);

                var x = umbracoHelper.TypedMedia(content.GetPropertyValue(fieldName));

                var imageUrl = umbracoHelper.TypedMedia(content.GetPropertyValue(fieldName)).Url;

                return new MvcHtmlString($@"<img src=""{imageUrl}"" />");
            }
            return MvcHtmlString.Empty;
        }


        /// <summary>
        /// returns the contents for the ids passed, returns all or the number passed in toTake
        /// </summary>
        /// <param name="content"></param>
        /// <param name="ids"></param>
        /// <param name="toTake"></param>
        /// <returns></returns>
        public static IEnumerable<IPublishedContent> GetContentFromIds(this UmbracoHelper content, object ids, int toTake = 0)
        {
            var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);

            if (toTake > 0)
            {
                return umbracoHelper.TypedContent((ids ?? "").ToString().Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).Take(toTake));
            }

            return umbracoHelper.TypedContent((ids ?? "").ToString().Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries));

        }
    }
}
