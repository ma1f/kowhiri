using System;
using System.Linq;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Kowhiri.SeleniumTests
{
    /// <summary>
    /// Helper methods for the selenium tests.
    /// </summary>
    internal static class ExtensionMethods
    {

        public static IWebElement FindElement(this IWebDriver driver, By by, int timeoutInSeconds = 3)
        {
            if (timeoutInSeconds > 0)
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeoutInSeconds));
                return wait.Until(drv => drv.FindElement(by));
            }
            return driver.FindElement(by);
        }

        /// <summary>
        /// Logs in with the user specified in the user.config settings.
        /// Logs out first if currently logged in.
        /// </summary>
        /// <param name="driver">IWebDriver to perform the action.</param>
        /// <param name="baseUrl">Base URL for the site (from setting 'BaseURL').</param>
        /// <param name="userEmailAddress">User email address (from setting 'TestUser').</param>
        /// <param name="password">Password for the user (from setting 'TestUserPassword').</param>
        public static void LogIn(this IWebDriver driver, string baseUrl, string userEmailAddress = "",
            string password = "")
        {
            driver.Navigate().GoToUrl(baseUrl + "/Authentication/Logout");
            driver.WaitUntilElementExistsInDom(By.Id("Email"));
            driver.FindElement(By.Id("Email")).Clear();
            driver.FindElement(By.Id("Email")).SendKeys(userEmailAddress);
            driver.FindElement(By.Id("Password")).Clear();
            driver.FindElement(By.Id("Password")).SendKeys(password);
            driver.FindElement(By.CssSelector("input[type=\"submit\"]")).Click();
           // WaitForTextToAppear(driver, By.CssSelector("h1"), "PULSE", true);
            //Assert.AreEqual("Pulse", driver.Title);
        }


        /// <summary>
        /// Hits the 'cancel' button on a pop-up.
        /// </summary>
        /// <param name="driver">IWebDriver to perform the action.</param>
        public static void CloseForm(this IWebDriver driver)
        {
            TryUntil(() =>
            {
                driver.FindElement(By.CssSelector("button.cancel")).Click();
                Thread.Sleep(200);
            });
        }

        /// <summary>
        /// Pauses execution until the alert message appears at the top of the screen.
        /// </summary>
        /// <param name="driver">IWebDriver to perform the action.</param>
        public static void WaitForAlertMessage(this IWebDriver driver)
        {
            var alert = By.CssSelector("div.ns-box-inner");
            driver.WaitUntilJavascriptElementIsPresent(alert, 20);
        }

        /// <summary>
        /// Sets a slider's value.
        /// </summary>
        /// <param name="driver">IWebDriver to perform the action.</param>
        /// <param name="fieldName">Id of the slider input field.</param>
        /// <param name="value">Value to set.</param>
        public static void SetSliderValue(this IWebDriver driver, string fieldName, int value)
        {
            // Set the slider value.
            var js = driver as IJavaScriptExecutor;
            js.ExecuteScript($"$('input[type=\"range\"]#{fieldName}').val({value}).change();");

            // Wait for it to update.
            Thread.Sleep(250);
        }


      

        public static void WaitForTextToAppear(this IWebDriver driver, By by, string textToWait, bool waitForDisplay)
        {
            TryUntil(() =>
            {
                var elements = driver.FindElements(by);
                Assert.IsNotNull(elements);
                Assert.IsTrue(elements.Count(el => el.Text.Contains(textToWait)) > 0 == waitForDisplay, "Text {0} didn't (dis)appear", textToWait);
            },30
                );
        }

        public static TResult TryUntil<TResult>(Func<TResult> assertion, int timesToRetry = 50,
            int millisecondsToWaitBetween = 200)
        {
            for (int i = 0; i < timesToRetry - 1; i++)
            {
                try
                {
                    return assertion();
                }
                catch (StaleElementReferenceException)
                {
                    Thread.Sleep(millisecondsToWaitBetween);
                }
                catch (NoSuchElementException)
                {
                    Thread.Sleep(millisecondsToWaitBetween);
                }
                catch (AssertionException)
                {
                    Thread.Sleep(millisecondsToWaitBetween);
                }
                catch (InvalidOperationException)
                {
                    Thread.Sleep(millisecondsToWaitBetween);
                }
            }
            return assertion();
        }


        public static void TryUntil(Action assertion, int timesToRetry = 10, int millisecondsToWaitBetween = 500)
        {
            TryUntil(() =>
            {
                assertion();
                return true;
            }, timesToRetry, millisecondsToWaitBetween);
        }


        public static void WaitUntilAnimationIsDone(this IWebDriver driver, string elementId, int retries = 50,
            int timeoutMs = 100)
        {
            bool complete = false;
            for (int i = 0; i < retries; i++)
            {
                var javaScriptExecutor = (IJavaScriptExecutor) driver;
                complete =
                    (bool) javaScriptExecutor.ExecuteScript(String.Format("return $('{0}').is(':animated')", elementId));
                if (complete)
                    return;
                Thread.Sleep(timeoutMs);
            }
            Assert.IsFalse(complete, "Animation didn't complete within timeout specified.");

        }

        public static void WaitForAttribute(this IWebDriver driver, By by, string attributeName, string attributeValue,
            int retries = 50, int timeoutMs = 100)
        {
            TryUntil(() =>
            {
                Assert.AreEqual(driver.FindElement(by).GetAttribute(attributeName), attributeValue);
                Thread.Sleep(timeoutMs);

            }, retries, timeoutMs);
        }


        public static void WaitForAjax(this IWebDriver driver)
        {
            TryUntil(() =>
            {
                Assert.AreEqual(true, (bool) (driver as IJavaScriptExecutor).ExecuteScript("return jQuery.active == 0"));

            }
                );

        }

      

       

        /// <summary>
        /// Selects an option by value.
        /// </summary>
        /// <param name="driver">IWebDriver to perform the action.</param>
        /// <param name="id">Id of the select control on the form.</param>
        /// <param name="value">Value to select.</param>
        public static void SelectByValue(this IWebDriver driver, string id, string value)
        {
            var selectElement = driver.FindElement(By.Id(id));
            var selectControl = new SelectElement(selectElement);
            selectControl.SelectByValue(value);
        }

        public static void SelectByText(this IWebDriver driver, string id, string text)
        {
            var selectElement = driver.FindElement(By.Id(id));
            var selectControl = new SelectElement(selectElement);
            selectControl.SelectByText(text);
        }

        /// <summary>
        /// Sets a text or textarea control's value.
        /// </summary>
        /// <param name="driver">IWebDriver to perform the action.</param>
        /// <param name="id">Id of the control to set.</param>
        /// <param name="value">Value to set.</param>
        public static void SetInputText(this IWebDriver driver, string id, string value)
        {
            var by = By.Id(id);
            var element = driver.FindElement(by);

            // This doesn't seem to work on input type='number' so we send Ctrl+a first to select the entire contents.
            //element.Clear();
            element.SendKeys(Keys.Control + "a");
            element.SendKeys(value);
        }

        /// <summary>
        /// Sets a checkbox/radio button to 'checked'
        /// </summary>
        /// <param name="driver">IWebDriver to perform the action.</param>
        /// <param name="id">Id of the control to set.</param>
        public static void SetChecked(this IWebDriver driver, string id)
        {
            // We need to click the label that sits on top of the radio/checkbox.
            var radio = driver.FindElement(By.Id(id));
            var parent = radio.GetParent();
            var label = parent.FindElement(By.TagName("label"));
            label.Click();

            //driver.SetAttribute(id, "checked", "checked");
        }

        public static void SetPermChecked(this IWebDriver driver, string name, string cssPath)
        {
            var radio = driver.FindElement(By.Name(name));
            var parent = radio.GetParent();
            var radioID = parent.FindElement(By.CssSelector(cssPath));
            radioID.Click();
        }


        /// <summary>
        /// Sets control's attribute
        /// </summary>
        /// <param name="driver">IWebDriver to perform the action.</param>
        /// <param name="id">Id of the control to set.</param>
        /// <param name="attribute">Name of the attribute to be set.</param>
        /// <param name="value">Value for the attribute</param>
        public static void SetAttribute(this IWebDriver driver, string id, string attribute, string value)
        {
            var js = driver as IJavaScriptExecutor;
            js.ExecuteScript(String.Format("$('#{0}').attr('{1}', '{2}'); $('#{0}').change();", id, attribute, value));
        }

        public static IWebElement GetParent(this IWebElement e)
        {
            return e.FindElement(By.XPath(".."));
        }

        /// <summary>
        /// Sets a hidden field's value.
        /// </summary>
        /// <param name="driver">IWebDriver to perform the action.</param>
        /// <param name="id">Id of the field to set.</param>
        /// <param name="value">Value for the field.</param>
        public static void SetValue(this IWebDriver driver, string id, string value)
        {
            var js = driver as IJavaScriptExecutor;
            var script = String.Format("$('#{0}').val('{1}');", id, value);
            js.ExecuteScript(script);
        }

     
        /// <summary>
        /// Returns the value of an input field.
        /// </summary>
        /// <param name="driver">IWebDriver to perform the action.</param>
        /// <param name="by">Selector for the input control.</param>
        /// <returns>Text value of the input control.</returns>
        public static string GetInputFieldValue(this IWebDriver driver, By by)
        {
            return driver.FindElement(by).GetAttribute("value");
        }

        /// <summary>
        /// Save Entry Button to submit the form.
        /// </summary>
        /// <param name="driver">IWebDriver to perform the action.</param>
        public static void SaveEntrySubmitButton(this IWebDriver driver)
        {
            // Using ClickElement which waits until the element is enabled 
            // and visible before trying to click it.
            driver.ClickElement(By.CssSelector("input[type=\"submit\"]"));
        }

        /// <summary>
        /// Save Button to save/submit 
        /// So far, only instance is /PatientAccount/Settings preferences page
        /// </summary>
        /// <param name="driver"></param>
        public static void SaveButton(this IWebDriver driver)
        {
            driver.FindElement(By.CssSelector("button[type=\"submit\"]")).Click();
        }

      

        /// <summary>
        /// Waits for the specified element to appear in the DOM.
        /// </summary>
        /// <param name="driver">IWebDriver to perform the action.</param>
        /// <param name="by">Selector to wait for.</param>
        /// <param name="timeout">Number of seconds to wait.</param>
        /// <returns>true if the element appears within the time specified.</returns>
        public static bool WaitUntilElementExistsInDom(this IWebDriver driver, By by, int timeout = 10)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeout));
            return wait.Until(d => d.IsElementPresent(by));
        }

        public static bool WaitUntilElementDoesntExistInDom(this IWebDriver driver, By by, int timeout = 10)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeout));
            return wait.Until(d => !d.IsElementPresent(by));
        }

        public static void WaitUntilElementVisible(this IWebDriver driver, By by, bool visible = true, int timeout = 10)
        {
            TryUntil(() =>
            {
                Console.WriteLine(DateTime.Now.ToString("G"));
                foreach (var elem in driver.FindElements(by))
                {
                    Console.WriteLine(elem.Text);
                    Console.WriteLine(elem.Displayed.ToString());
                }
                Assert.IsTrue(driver.FindElements(by).Count(el => el.Displayed.Equals(visible)) > 0, "Element displayed property hasn't become {0}", visible);
                Console.WriteLine(DateTime.Now.ToString("G"));
            }
                );

        }

    /// <summary>
        /// Waits for the specified element to appear in the DOM and have something in it.
        /// </summary>
        /// <param name="driver">IWebDriver to perform the action.</param>
        /// <param name="by">Selector to wait for.</param>
        /// <param name="timeout">Number of seconds to wait.</param>
        /// <returns>true if the element appears within the time specified.</returns>
        public static bool WaitUntilElementIsPresentAndNotEmpty(this IWebDriver driver, By by, int timeout = 10)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeout));
            WaitUntilElementExistsInDom(driver, by);
            var element = driver.FindElement(by);

            TryUntil(() =>
                    {
                        Assert.IsNotEmpty(element.Text, "Element text is empty.");
                    }
                );
            return wait.Until(d => (element.Text.Length > 0) );
        }

        /// <summary>
        /// Waits for the specified element to appear in the DOM and be clickable.
        /// </summary>
        /// <param name="driver">IWebDriver to perform the action.</param>
        /// <param name="by">Selector to wait for.</param>
        /// <param name="timeout">Number of seconds to wait.</param>
        /// <returns>true if the element is clickable.</returns>
        public static bool WaitUntilElementIsClickable(this IWebDriver driver, By by, int timeout = 10)
        {            
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeout));
            WaitUntilElementExistsInDom(driver, by);
            var element = driver.FindElement(by);
            return wait.Until(d => (element.Displayed && element.Enabled));

        }

        /// <summary>
        /// Waits for the lhd.page.elementId to be populated with a value > 0.
        /// </summary>
        /// <param name="driver">IWebDriver to perform the action.</param>
        /// <param name="timeout">Number of seconds to wait.</param>
        /// <returns>Id of the entity if it appears within the time specified.</returns>
        public static int? WaitUntilEntityIdIsPresent(this IWebDriver driver, int timeout = 10)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeout));

            var js = driver as IJavaScriptExecutor;
            int? result = null;
            wait.Until(d =>
            {
                
                var elementId = (int)(long)js.ExecuteScript("return lhd.page.lastModifiedEntityId");
                if (elementId > 0)
                {
                    result = elementId;
                }

                // Did we get a value?
                return (result.HasValue);
            });

            return result;
        }


        /// <summary>
        /// Waits for either of the specified elements to appear in the DOM.
        /// </summary>
        /// <param name="driver">IWebDriver to perform the action.</param>
        /// <param name="by1">Selector to wait for.</param>
        /// <param name="by2">Selector to wait for.</param>
        /// <param name="timeout">Number of seconds to wait.</param>
        /// <returns>true if the element appears within the time specified.</returns>
        public static bool WaitUntilEitherElementIsPresent(this IWebDriver driver, By by1, By by2, int timeout = 10)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeout));
            return wait.Until(d => d.IsElementPresent(by1) || d.IsElementPresent(by2));
        }

        /// <summary>
        /// Waits until the specified element is no longer present in the DOM.
        /// </summary>
        /// <param name="driver">IWebDriver to perform the action.</param>
        /// <param name="by">Selector to watch.</param>
        /// <param name="timeout">Maximum number of seconds to wait.</param>
        /// <returns>true if the element disappears within the specified time.</returns>
        public static bool WaitUntilElementIsNotPresent(this IWebDriver driver, By by, int timeout = 10)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeout));
            return wait.Until(d => d.IsElementNotPresent(by));
        }



        /// <summary>
        /// Same as wait until element is present but with more frequent polling.
        /// </summary>
        /// <param name="driver">IWebDriver to perform the action.</param>
        /// <param name="by">Selector to watch.</param>
        /// <param name="timeout">Maximum number of seconds to wait.</param>
        /// <returns>true if the element disappears within the specified time.</returns>
        public static bool WaitUntilJavascriptElementIsPresent(this IWebDriver driver, By by, int timeout = 10)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeout))
            {
                PollingInterval = TimeSpan.FromMilliseconds(100)
            };
            return wait.Until(d => d.IsElementPresent(by));
        }

        /// <summary>
        /// Returns true if the specified element is present in the DOM.
        /// </summary>
        /// <param name="driver">IWebDriver to perform the action.</param>
        /// <param name="by">Element to query for.</param>
        /// <returns>true if the element is present.</returns>
        public static bool IsElementPresent(this IWebDriver driver, By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        /// <summary>
        /// Returns true if the specified element cannot be found in the DOM.
        /// </summary>
        /// <param name="driver">IWebDriver to perform the action.</param>
        /// <param name="by">Element to query for.</param>
        /// <returns>true if the element is not present in the DOM.</returns>
        public static bool IsElementNotPresent(this IWebDriver driver, By by)
        {
            return !driver.IsElementPresent(by);
        }

    

        /// <summary>
        /// Finds an element, waiting until it is enabled and visible then clicks it.
        /// </summary>
        /// <param name="driver">IWebDriver to perform the action.</param>
        /// <param name="by">Selector for element.
        /// <param name="timeout">Time oput in seconds.</param>
        public static void ClickElement(this IWebDriver driver, By by, int timeout = 10)
        {
            var js = driver as IJavaScriptExecutor;
            WaitUntilElementIsClickable(driver, by);
            var element = driver.FindElement(by);
            js.ExecuteScript("arguments[0].click();", element);
        }

    }
}
