using System;
using NUnit.Framework;
using OpenQA.Selenium;

namespace Kowhiri.SeleniumTests
{
    [TestFixture]
    public class ContactTests : TestBaseSetup
    {
        [OneTimeSetUp]
        public void SetupTest()
        {
            Setup();
        }

        [OneTimeTearDown]
        public void TeardownTest()
        {
            Teardown();
        }


        [Test]
        public void ContactFormWontSubmitWithoutEmail()
        {

            Driver.Navigate().GoToUrl(BaseUrl + "/contact");

            Driver.WaitUntilElementIsClickable(By.Id("FirstName"));

            Driver.FindElement(By.Id("FirstName")).SendKeys("Test First Name");
            Driver.FindElement(By.Id("LastName")).SendKeys("Test Last Name");
            Driver.FindElement(By.Id("PhoneNumber")).SendKeys("Test Phone Number 123");
            Driver.FindElement(By.Id("Enquiry")).SendKeys("Test Enquiry");

            Driver.SaveEntrySubmitButton();
            Driver.WaitForTextToAppear(By.CssSelector(".field-validation-error > span"), "The Email field is required.", true);

        }


        [Test]
        public void ContactFormSubmits()
        {
            Driver.Navigate().GoToUrl(BaseUrl + "/contact");

            Driver.WaitUntilElementIsClickable(By.Id("FirstName"));

            Driver.FindElement(By.Id("FirstName")).SendKeys("Test First Name");
            Driver.FindElement(By.Id("LastName")).SendKeys("Test Last Name");
            Driver.FindElement(By.Id("PhoneNumber")).SendKeys("Test Phone Number 123");
            Driver.FindElement(By.Id("Email")).SendKeys("test@email.com");
            Driver.FindElement(By.Id("Enquiry")).SendKeys("Test Enquiry");

            Driver.SaveEntrySubmitButton();
            Driver.WaitUntilElementExistsInDom(By.ClassName("message-sent-title"), 5);

        }

        
    }
}
