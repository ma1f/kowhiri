using System;
using System.Drawing.Imaging;
using System.IO;
using System.Threading;
using Kowhiri.SeleniumTests.Properties;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.Events;
using OpenQA.Selenium.Support.Extensions;

namespace Kowhiri.SeleniumTests
{
   
    /// <summary>
    /// Base class for module tests.
    /// </summary>
    public class TestBaseSetup
    {
        #region Data Members
        /// <summary>
        /// IWebDriver used to perform the tests.
        /// </summary>
        public IWebDriver Driver { get; private set; }

        /// <summary>
        /// Base URL of the site being tested.
        /// </summary>
        public string BaseUrl { get; private set; }

        /// <summary>
        /// Module currently being tested.
        /// </summary>
        public string CurrentModule { get; set; }

        #endregion

        /// <summary>
        /// Constructor so we can set defaults.
        /// </summary>
        public TestBaseSetup()
        {
            CurrentModule = "Unknown";
        }

        /// <summary>
        /// Starts the web driver.
        /// </summary>
        /// <param name="url"></param>
        protected void Setup(string url = null)
        {
            // Create the driver.
            var options = new ChromeOptions();
            options.AddArgument("--start-maximized");
            options.AddArgument("--test-type");

            // Set up the options.
            var service = ChromeDriverService.CreateDefaultService(Settings.Default.ChromeDriverPath);
            service.LogPath = "chromedriver.log";
            service.EnableVerboseLogging = true;

            // Create an event firing webdriver.
            var firingDriver = new EventFiringWebDriver(new ChromeDriver(Settings.Default.ChromeDriverPath, options, TimeSpan.FromSeconds(180)));
            //firingDriver.ExceptionThrown += TakeScreenshotOnException;
            Driver = firingDriver;


            // Set the base URL.)
            BaseUrl = url ?? Settings.Default.BaseUrl;
            Driver.Manage().Window.Maximize();

            Driver.Navigate().GoToUrl(BaseUrl);

            //// Make sure we're logged in.
            //if (userEmailAddress.Equals(String.Empty) || password.Equals(String.Empty))
            //    return;

            // Driver.LogIn(BaseUrl, userEmailAddress, password);
        }


        /// <summary>
        /// Quits the web driver.
        /// </summary>
        protected void Teardown()
        {
            try
            {
                Driver.Quit();
            }
            catch
            {
                // Ignore errors if unable to close the browser
            }
        }

        /// <summary>
        /// Navigates to a module page.
        /// Asserts that the module page name is correct.
        /// Note: navigation is by URL, not via the menus.
        /// </summary>
        /// <param name="pageUrl">Relative Url of the module page.</param>
        /// <param name="moduleName">Title of the module.</param>
        public void NavigateToPage(string pageUrl, string moduleName)
        {

            // Make sure we don't have two '//'.
            var fullUrl = BaseUrl;
            if (fullUrl.EndsWith("/"))
            {
                fullUrl = fullUrl.TrimEnd('/');
            }
            if (!pageUrl.StartsWith("/"))
            {
                pageUrl = "/" + pageUrl;
            }
            fullUrl += pageUrl;

            // Go to the specified module page.
            Driver.Navigate().GoToUrl(fullUrl);

            // Make sure we're on the correct page.
            Driver.WaitForTextToAppear(By.CssSelector("h1"), moduleName.ToUpper(), true);
            // Wait for the results grid to load.
        }

        /// <summary>
        /// Take a screen shot everytime Selenium has an exception
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Exception.</param>
        private void TakeScreenshotOnException(object sender, WebDriverExceptionEventArgs e)
        {
            // Find all existing  screenshot files for this module.
            string searchTerm = String.Format("{0}*.png", CurrentModule);
            var files = Directory.EnumerateFiles(Environment.CurrentDirectory, searchTerm);

            // Delete files.
            foreach (string fileName in files)
            {
                try
                {
                    File.Delete(fileName);
                }
                catch (IOException)
                {
                    // Wait a bit then try again.
                    Thread.Sleep(1000);
                    File.Delete(fileName);
                }
            }

            // Create a new one with the latest exception.
            string timestamp = DateTime.Now.ToString("yyyy-MM-dd-hhmm-ss");
            Driver.TakeScreenshot().SaveAsFile(CurrentModule + "-" + timestamp + ".png", ImageFormat.Png);

        }

    }
}
