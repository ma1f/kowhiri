using NUnit.Framework;
using OpenQA.Selenium;

namespace Kowhiri.SeleniumTests
{
    [TestFixture]
    public class SmokeTests : TestBaseSetup
    {
        [OneTimeSetUp]
        public void SetupTest()
        {
            Setup();
        }

        [OneTimeTearDown]
        public void TeardownTest()
        {
            Teardown();
        }

        [Test]
        public void TestNavigation()
        {
            Driver.WaitForTextToAppear(By.CssSelector("h1"), "Welcome!", true);
            NavigateToPage("/Contact", "Contact");
            NavigateToPage("/about-us", "About Us");
            NavigateToPage("/faqs", "Faqs");
            NavigateToPage("/error-404", "Error - 404");

        }

    }
}
